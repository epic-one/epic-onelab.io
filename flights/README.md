### Flights API README

* The goal here is to enlisht the abstraction end points for the Flights API that this backend microservice exposes to the front end epic mobile app.

### Base URL for all APIs
http://platform.epic.one

#### We will enlist all APIs here, preferably in the order of the flow. If there are any dependencies amongst the APIs the dependencies will be highlighted as well

* This is the API for obtaining tickets between a pair of cities for a one way journey
`GET /flights/from/to/no_of_adults=&no_of_children=&class_of_travel&date=`

##### Sample response
* ```
    {"results":[
        {
            "from":" ",
            "to":" ",
            "no_of_stops": " ",
            "airline_name": " ",
            "via details": " " ,
            "arrival_date": " ",
            "arrival_time": " ",
            "departure_time": " ",
            "seat_available": " ",
            "flight_key": " ",
            "duration": " ",
            "base_fare": " ",
            "taxes": " ",
            "total": " "
        }
    ]}
```